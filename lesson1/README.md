デザイナー向けのGit講座 #1

##テーマ: コンフリクトとマージ
###目標: ローカル環境でのマージとコンフリクト解決

* 11:00〜 環境準備
* 11:15〜 基本用語解説
* 11:30〜 実践
* 11:50〜 まとめ

## 1.環境準備
* 各マシンにリポジトリをクローン
* リポジトリに招待

## 2.基本用語

* git
* bitbucket

コマンド

* add
* commit
* pull
* push
* branch
* clone

## 3. 実践

1.cloneする

 ssh `git@bitbucket.org:n0bisuke/git.practice.git`
 https `https://n0bisuke@bitbucket.org/n0bisuke/git.practice.git`

###空のディレクトリ作成
![](http://i.gyazo.com/892c9d0db8f9856b5c2f355503423009.png)

###URLを指定
![](http://i.gyazo.com/5cdfade27fb4a85f8a52264244bf756a.png)

###クローン完了
![](http://i.gyazo.com/11659b22140621a813a33c4f399d2ba5.png)

2. ./1/README.md を編集

3. push

4. コンフリクト発生と解決

## 4. まとめ
* おさらい
* 質疑応答
* 次回の日程と内容決め

## その他 (時間があれば)
画像差分について

![](http://i.gyazo.com/32a5b61770287a2eefc15ad5da6f4517.png)

![](http://i.gyazo.com/3ae93849ad5a71a4517304cdb8e4c251.png)
